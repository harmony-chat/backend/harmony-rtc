const r = require("./rethink");
const {checkLogin} = require("./token");

module.exports = {
  async checkToken(token) {
    const userId = token.substring(0, token.indexOf("."));
    const user = await r
      .table("users")
      .get(userId)
      .run(r.conn);
    if (!user) return null;
    if (checkLogin(token, user.passwordHash, userId)) return userId;
    else return null;
  }
};
