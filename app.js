// Mediasoup seems to catch these and kill server silently...
process.on("unhandledRejection", err => {
  console.error(err);
  throw err;
});

process.on("uncaughtException", err => {
  console.error(err);
  throw err;
});

const mediasoup = require("mediasoup");
const env = require("./env");
const constants = require("./constants");
const WebSocket = require("@discordjs/uws");
const msgpack = new (require("msgpack5"))();
const express = require("express");
const app = express();
const utils = require("./utils");
const randomBytes = require("util").promisify(require("crypto").randomBytes);

const wss = new WebSocket.Server({
  port: 3010
});

app.listen(3009, function() {
  console.log("Harmony RTC listening on REST 3009 and WSS 3010");
});

const server = mediasoup.Server({
  logLevel: "debug",
  logTags: ["info", "ice", "dtls", "rtp", "srtp", "rtcp", "rtx", "rbe"],
  // true = autodetect, false = disabled
  rtcIPv4: env.privateAddress.v4 || env.publicAddress.v4 !== undefined,
  rtcIPv6: env.privateAddress.v6 || env.publicAddress.v6 !== undefined,
  rtcAnnouncedIPv4: env.publicAddress.v4,
  rtcAnnouncedIPv6: env.publicAddress.v6,
  rtcMinPort: env.portRange.min,
  rtcMaxPort: env.portRange.max
  // TODO: dtlsCertificateFile,dtlsPrivateKeyFile
});

const codecs = [
  {
    kind: "audio",
    name: "opus",
    clockRate: 48000,
    channels: 2,
    parameters: {
      useinbandfec: 1
    }
  },

  // We'll have to figure this out at some point lmao
  {
    kind: "video",
    name: "VP8",
    clockRate: 90000
  },
  {
    kind: "video",
    name: "H264",
    clockRate: 90000,
    parameters: {
      "packetization-mode": 1,
      "profile-level-id": "42e01f",
      "level-asymmetry-allowed": 1
    }
  }
];

server.on("close", function() {
  console.warn("RTC Server just closed, preparing for shutdown");
  // wss.close();
  // TODO: Actually shut stuff down and mark us as unavailable etc
  process.exit();
});

const rooms = new Map();
// userId:roomCount
const allowedUsers = new Map();
// channelId:[userId]
const roomAllowedUsers = new Map();
// sessionId:WS
const connections = new Map();
// userId:sessionId
const userIds = new Map();

app.delete("/rooms/:roomId/peers/:peerId", async function(req, res) {
  // Disconnect a user from a room
  const room = rooms.get(req.params.roomId);
  if (room) {
    const peer = room.getPeerByName(req.params.peerId);
    if (peer) {
      peer.close();
    } else
      console.warn(
        `Room ${req.params.roomId} doesn't have peer ${req.params.peerId}`
      );
  } else console.warn(`Room ${req.params.roomId} doesn't exist`);
  res.status(204).send();
});

app.put("/rooms/:roomId/peers/:peerId", async function(req, res) {
  console.log("putting peer into room!");
  if (rooms.has(req.params.roomId)) {
    console.log("Room exists");
    roomAllowedUsers.get(req.params.roomId).add(req.params.peerId);
  } else {
    console.log("Creating room");
    const room = server.Room(codecs);
    rooms.set(req.params.roomId, room);
    room.on("newpeer", function(peer) {
      const sessionId = userIds.get(peer.name);
      console.log("new peer", peer);
      peer.on("notify", function(notification) {
        console.log("notify", notification);
        sendOp(sessionId, constants.ops.mediasoup, {
          c: req.params.roomId,
          d: notification
        });
      });
      peer.on("close", function() {
        const existingCount = allowedUsers.get(peer.name);
        if (existingCount !== undefined) {
          if (existingCount <= 1) allowedUsers.delete(peer.name);
          else allowedUsers.set(peer.name, existingCount - 1);
        }

        roomAllowedUsers.get(req.params.roomId).delete(peer.name);
      });
      const session = connections.get(sessionId);
      session.ws.on("close", () => {
        if (peer && !peer.closed && !room.closed) peer.close();
      });
    });
    roomAllowedUsers.set(req.params.roomId, new Set([req.params.peerId]));
  }
  console.log("Adding peer in");
  allowedUsers.set(
    req.params.peerId,
    (allowedUsers.get(req.params.peerId) || 0) + 1
  );
  console.log("Done");

  res.status(204).send();
});

wss.on("connection", function(ws) {
  ws.once("message", async function(token) {
    try {
      var userId = await utils.checkToken(token);
      if (userId === null) {
        console.log("userId === null");
        return ws.close();
      }
      if (!allowedUsers.get(userId)) {
        console.log("Not allowed");
        return ws.close();
      }
    } catch (err) {
      console.warn(err);
      return ws.close();
    }

    const sessionId = (await randomBytes(16)).toString("base64");

    // TODO: We should probably support multiple sessions from one user
    const sessionObject = {
      userId,
      sessionId,
      rooms: new Map(),
      pingInterval: setInterval(() => {
        sendOp(sessionId, constants.ops.heartbeat);
        // Otherwise we end up orphaning timeouts
        if (!sessionObject.killTimeout) {
          sessionObject.killTimeout = setTimeout(() => {
            console.log("killTimeout ticked, killing session");
            close(sessionId, constants.codes.heartbeatFailed);
          }, 30000);
        }
      }, 5000),

      ws
    };

    connections.set(sessionId, sessionObject);
    userIds.set(userId, sessionId);
    sendOp(sessionId, constants.ops.hello, {
      h: 5000
    });

    ws.on("message", frame => onMessage(sessionId, frame));
  });
});

function sendOp(sessionId, op, data) {
  const session = connections.get(sessionId);
  console.log(sessionId, "->", {o: op, d: data});
  const frame = msgpack.encode({
    o: op,
    d: data
  });
  session.ws.send(frame);
}

function close(sessionId, code) {
  // We need to do a lot more cleanup here, but this will do for now
  const session = connections.get(sessionId);

  if (session.killTimeout) clearTimeout(session.killTimeout);
  if (session.pingInterval) clearInterval(session.pingInterval);
  // for (const room of session.rooms.values()) {
  //   const peer = room.getPeerByName(session.userId);
  //   if (peer) peer.close();
  // }
  session.ws.close(code);
}

async function onMessage(sessionId, frame) {
  try {
    var msg = msgpack.decode(frame);
  } catch (err) {
    console.warn(err);
    return close(sessionId, constants.codes.decodeFailed);
  }

  console.log(sessionId, "<-", msg);

  const session = connections.get(sessionId);

  switch (msg.o) {
    case constants.ops.heartbeat: {
      if (session.killTimeout !== null) {
        clearTimeout(session.killTimeout);
        session.killTimeout = null;
      }
      break;
    }

    case constants.ops.mediasoup: {
      // {c:roomId/channelId,d:request}
      console.log("Got a mediasoup request!", msg.d.d);
      if (!msg.d.c || !msg.d.d)
        return close(sessionId, constants.codes.invalidMediasoupOp);
      const roomId = msg.d.c;
      const allowed = roomAllowedUsers.get(roomId);
      console.log("Is allowed?", allowed);
      if (!allowed || !allowed.has(session.userId))
        return close(sessionId, constants.codes.forbiddenRoom);

      const room = rooms.get(roomId);
      console.log("Got our room", room);

      if (msg.d.d.target == "room" && !msg.d.d.notification) {
        if (
          msg.d.d.method == "join" &&
          (msg.d.d.spy || msg.d.d.peerName !== session.userId)
        ) {
          return close(sessionId, constants.codes.mediasoupRestrictions);
        }
        try {
          console.log("Receiving room request", msg.d.d);
          const response = await room.receiveRequest(msg.d.d);
          console.log("Got a response!", response);
          sendOp(sessionId, constants.ops.mediasoup, {
            c: roomId,
            d: response,
            n: msg.d.n
          });
        } catch (err) {
          console.warn(err);
          return close(sessionId, constants.codes.mediasoupError);
        }
      } else if (msg.d.d.target == "peer") {
        const peer = room.getPeerByName(session.userId);
        if (!peer) return close(sessionId, constants.codes.forbiddenRoom);

        console.log("Receiving peer request", msg.d.d);
        if (msg.d.d.notification) {
          peer.receiveNotification(msg.d.d);
        } else {
          try {
            const response = await peer.receiveRequest(msg.d.d);
            sendOp(sessionId, constants.ops.mediasoup, {
              c: roomId,
              d: response,
              n: msg.d.n
            });
          } catch (err) {
            console.warn(err);
            return close(sessionId, constants.codes.mediasoupError);
          }
        }
      }
      break;
    }
    default: {
      return close(sessionId, constants.codes.invalidOp);
    }
  }
}
