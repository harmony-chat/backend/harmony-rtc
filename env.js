module.exports = {
  publicAddress: {
    v4: process.env.PUBLIC_V4,
    v6: process.env.PUBLIC_V6
  },
  privateAddress: {
    v4: process.env.PRIVATE_V4,
    v6: process.env.PRIVATE_V6
  },
  portRange: {
    min: 6000,
    max: 7600
  },
  rethink: {
    host: process.env.RETHINK_HOST || "localhost",
    port: process.env.RETHINK_PORT || 28015,
    user: process.env.RETHINK_USER || "admin",
    password: process.env.RETHINK_PASSWORD || "",
    get db() {
      return `harmony_${module.exports.deployment}`;
    }
  },
  serverSalt: process.env.SERVER_SALT || "salt",
  deployment: process.env.NODE_ENV || "local"
};
