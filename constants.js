module.exports = {
  ops: {
    hello: 0,
    mediasoup: 1,
    heartbeat: 2
  },
  codes: {
    mediasoupError: 40001
  }
};
